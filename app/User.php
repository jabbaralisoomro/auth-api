<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',"password",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected function rules(){
        return [
            'signUp' => [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => [
                    'required',
                    'string'
                ],


            ],
            'login' => [
                'email' => 'required|email',
                'password' => [
                    'required',
                    'string'
                ],


            ],
        ];
    }
    protected function createUser($user){

        return $this->create($user);
    }

    protected function getUser($email,$password){

        $loginUser=null;

        $user = $this->where('email', $email)->first();

        if ($user) {

            if (Hash::check($password, $user->password)) {
                $loginUser=$user;
            }

        }
        return $user;
    }

    public function checkUserEmail($email)
    {
        return $this->where('email', $email)->first();
    }

    public function checkPassword($rawPassword, $hashPassword)
    {
        return \Illuminate\Support\Facades\Hash::check($rawPassword, $hashPassword);
    }
}
