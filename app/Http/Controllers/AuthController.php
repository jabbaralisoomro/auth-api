<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    protected $user;
    public $successStatus = 200;
    public $HTTP_FORBIDDEN = 403;
    public $HTTP_NOT_FOUND = 404;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->success = false;
        $this->data = false;
        $this->statusCode = 200;
        $this->msg = "";
        $this->user = new User();

    }

    /**
     * @OA\Post(
     * path="/auth/sign-up",
     * summary="Sign Up",
     * description="Sigh up",
     * operationId="authSignUp",
     * tags={"auth"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password","name"},
     *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *       @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *     @OA\Property(property="name", type="string",  example="jhon "),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */
    public function signUp(Request $request)
    {

        $validator = $this->validate($request, User::rules()['signUp']);


        if (!$validator) {
            return $this->sendResponse(422, $this->success, __('message.validation_error'), $validator->errors());
        }


        try {
            $inputs = $request->all();

            $userCreate['email'] = $inputs['email'];
            $userCreate['name'] = $inputs['name'];
            $userCreate['password'] = Hash::make($inputs['password']);
            \DB::beginTransaction();
            $newUser = User::createUser($userCreate);
            DB::commit();
            if ($newUser) {
                $user = new User();
                $this->msg = __('message.user.created');
                $this->data = ['user' => $newUser];
                $this->success = true;
                \DB::beginTransaction();
                $this->data['token'] = $user::find($newUser->id)->createToken('UserApp')->accessToken;
                DB::commit();

            } else {
                $this->msg = __('message.user.fail');
                $this->statusCode = 400;

            }

        } catch (Exception $exception) {
            $this->msg = __('message.server_error');
            $this->statusCode = 500;
            DB::rollBack();
        } finally {

        }
        return $this->sendResponse($this->statusCode, $this->success, $this->msg, $this->data);
    }


    public function login2(Request $request)
    {
        $array = array();
        $user = $this->user->checkUserEmail($request->email);
        if (empty($user) || !$this->user->checkPassword($request->password, $user->password)) {
            return response()->json([
                'error' => false,
                'status' => 401,
                'message' => 'These credentials does not match our records.',
                'data' => []
            ]);
        }
        $tokenData = $user->createToken('UserApp');
        $array['access_token'] = $tokenData->accessToken;
        $array['token_type'] = 'Bearer';
        $array['expires_at'] = ($tokenData->token->expires_at)->format('Y-m-d H:i:s');
        $array['user'] = $user;
        return response()->json([
            'success' => true,
            'status' => $this->successStatus,
            'message' => 'Record found.',
            'data' => $array
        ]);
    }




    /**
     * @OA\Post(
     * path="/auth/login",
     * summary="Sign in",
     * description="Login by email, password",
     * operationId="authLogin",
     * tags={"auth"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password"},
     *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *       @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */

    public function login(Request $request)
    {

        $validator = $this->validate($request, User::rules()['login']);


        if (!$validator) {
            return $this->sendResponse(422, $this->success, __('message.validation_error'), $validator->errors());
        }

        $email = $request->get('email');
        $password = $request->get('password');

        try {
            $user = User::getUser($email, $password);
            if ($user) {
                $this->msg = __('message.user.login');
                $this->success = true;
                $this->data = ['user' => $user];
                $token = $user->createToken('UserApp');
                $this->data['token'] = $token->accessToken;

            } else {
                $this->statusCode = 422;
                $this->msg = __('message.user.invalid');
            }

        } catch (Exception $exception) {
            $this->msg = __('message.server_error');
            $this->statusCode = 500;

        } finally {
        }
        return $this->sendResponse($this->statusCode, $this->success, $this->msg, $this->data);
    }


    /**
     * @OA\Get(
     *  path="/auth/profile",
     *   tags={"Profile"},
     *  summary="Get profile",
     *  @OA\Response(response=200, description="Return profile"),
     *  @OA\Response(response=422, description="Invalid USer"),
     *  security={{ "bearerAuth": {} }}
     * )
     */
    public function getProfile(Request  $request)
    {

        $user = Auth::user();

        if ($user) {
            $profile = $user;
            $this->msg = __('message.user.profile');
            $this->success = true;
            $this->data = $profile;
        } else {
            $this->statusCode = 422;
            $this->msg = __('message.user.invalid');
        }

        //Send Response
        return $this->sendResponse($this->statusCode, $this->success, $this->msg, $this->data);
    }


    //
}
