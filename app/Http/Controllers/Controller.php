<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**

    @OA\Info(
    version="",
    title="Lumen - APIs",
    description="for more information please:",
    @OA\Contact(
    email="emad.jamil@nextgeni.net",
    name="Emad"
    )
    )
     **/


    /**
     *  @OA\Server(
     *      url=SWAGGER_LUME_CONST_HOST,
     *      description="dev"
     *  )
     */

    /**
    @OA\SecurityScheme(
    securityScheme="bearerAuth",
    type="http",
    scheme="bearer",
    bearerFormat="JWT"
    ),
     **/

    /**
     * @OA\SecurityScheme(
     *     type="apiKey",
     *     description="device type from which API is being called (web, ios, android)",
     *     name="x-device-type",
     *     in="header",
     *     securityScheme="device_type"
     * )
     */

    /**
     * @OA\SecurityScheme(
     *     type="apiKey",
     *     name="x-app-version",
     *     description="current app version, required for ios & android, optional for web",
     *     in="header",
     *     securityScheme="app-version"
     * )
     **/

    /**
     * @OA\SecurityScheme(
     *     type="apiKey",
     *     name="x-os-version",
     *     description="device current os version, required for ios & android, optional for web",
     *     in="header",
     *     securityScheme="os_version"
     * )
     **/


    public function sendResponse($statusCode,$success,$message,$data = false)
    {
        if(!$data)
            $data = new \stdClass();
        $response = [
            'status_code' => $statusCode,
            'success' => $success,
            'data'    => $data,
            'message' => $message,

        ];

        return response()->json($response, $statusCode);
    }

}
